# Perse Solutions

Here you will find solutions for the **2021 Round 1** Perse Coding Challenge.

Each question is tagged as EASY, MEDIUM, HARD or VERY HARD.

### Make sure to read the comments added to help explain each program.

There are also some bonus questions which range from MEDIUM to VERY HARD.