# Bonus - Question 2: **Goldbach Conjecture** [HARD]
---
It is known that all even numbers between 4 and 300,000,000,000,000,000 are equal to the sum of two primes.

For example, 30 can be made by 7 + 23, 11 + 19 or 13 + 17. These are the only ways of expressing 30 as a sum of two primes, as the order of the numbers in the additions does not matter.

Your program should input *n*, a single even number (4 ≤ *n* ≤ 10000), and should output the number of ways of which *n* can be expressed as a sum of two primes.

### Sample Case
|||
|----|----|
|*Input*|`22`|
|*Output*|**`3`**|

There are **5** test cases.