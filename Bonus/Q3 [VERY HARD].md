# Bonus - Question 3: **Island Geometry** [VERY HARD]
---
On a map, land and water are represented by 1s and 0s (land being a 1, water being a 0). For example:

```
1 1 1 0
1 1 0 0
0 0 0 1
0 0 0 0
```

*Note that land can only be connected horizontally or vertically - not diagonally.*

In this example, there are two islands, one with an area of 5, and one with an area of 1.

Given an *n* x *n* map (2 ≤ n ≤ 100), write a program that outputs the number of islands.

### Structure
The input will be *n* on the first line, followed by *n* lines of length *n*, representing each row of the map. Note that the 1s and 0s are separated by spaces.

You should output a single integer.

### Sample Case
|||
|----|----|
|*Input*|`4`|
||`1 1 1 0`|
||`1 1 0 0`|
||`0 0 0 1`|
||`0 0 0 0`|
|*Output*|**`2`**|