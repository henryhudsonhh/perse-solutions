# Bonus - Question 1: **Colour Encryption** [MEDIUM]
---
The process of colour encryption is carried out on a string made from three characters: `R`, `G`, `B`.

The encryption is carried out as follows:

• The first and last letters are ignored.

• 1. For the other letters, if the letters to the left and right of the letter are the same, then you should change the letter to that repeated letter.

• 2. If the letters to the right and left of the current letter are not equal, then the current letter is set as the letter which is missing out of `R`, `G` and `B` from the current three letters we are checking.


### Sample Case
|||
|-----|------|
|*Input*|`RGRBBG`|
|*Output*|**`RRGRBG`**|

### Explanation
We start with `RGRBBG`.

Looking at the second letter (ignore the first and last), the two adjacent letters are both `R`. Therefore we change the second letter to `R`.

We now have `RRRBBG`. Looking at the third letter, the two adjacent letters are `R` and `B`. Because these are not equal, we need to find which letter from `[R, G, B]` is missing from the adjacent letters. In this case, it's the `G`, so we make the third letter `G`.

We now have `RRGBBG`. For the fourth letter, we can see that the adjacent letters are `G` and `B`, so we set it to `R`.

We now have `RRGRBG`. The adjacent letters for the fifth letter are `R` and `G`, and the fifth letter is already `B`, so we don't need to change it.

We ignore the final letter.

### Structure
You will be given a single input of length *s* (3 ≤ *s* ≤ 2¹⁹), and your program should output the encrypted string.

There are **5** test cases.
