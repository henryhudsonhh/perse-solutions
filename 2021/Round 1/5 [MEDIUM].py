# This problem includes two different loops.

carrotSizes = [] # An empty list, which we will add to.
valid = 0
invalid = 0

minSize = int(input())
maxSize = int(input())

size = int(input())
while size != -1:
  carrotSizes.append(size)
  size = int(input())

for i in range(len(carrotSizes)):
  if carrotSizes[i] < minSize or carrotSizes[i] > maxSize:
    invalid += 1
  else:
    valid += 1

print(valid)
print(invalid)