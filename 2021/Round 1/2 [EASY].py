firstname = input()
surname = input()

namesCheck = sorted([surname, "Hopper"])
# This is really nice trick we can use - adding surname and "Hopper" into a list
# and then using the python sorted() function, which will sort the list alphabetically. 
# There are other ways to do this, but this one is the fastest.

if namesCheck[0] == surname: # surname comes before "Hopper" alphabetically as it's the first item in the sorted list.
    print(firstname, surname)
else: # surname comes after "Hopper" alphabetically
    print(surname + ', ' + firstname)