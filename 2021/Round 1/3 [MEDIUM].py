amountEatenByAmelie = 0
amelie = int(input())
bakshi = int(input())
chloe = int(input())

# Integer division means rounding down the result of a division to a whole number.
# For example, 5 // 2 (double slash means 5 integer division 2) is equal to 2
# Whereas 5 / 2 = 2.5

amountEatenByAmelie += amelie // 2
bakshi += (amelie - amelie // 2)
chloe += (bakshi - bakshi // 2)
amountEatenByAmelie += (chloe - chloe // 2)

print(amountEatenByAmelie)