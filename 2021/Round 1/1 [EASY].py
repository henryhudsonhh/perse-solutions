time = int(input())
print(5 * (time ** 2)) # Note: The '**' operator means 'power of', e.g. 3 ** 2 = 9.