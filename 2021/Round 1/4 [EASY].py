# There's quite a lot going on here which you may have not seen before.
# I'll do my best to explain it.

# Firstly, the len() function gets the length of a string, list or a set.
# A set is very similar to a list, but it deletes repeated elements.
# For example, a list of [1,1,2,3,3,4], when converted to a set, would be
# {1,2,3,4}. Sets also use different brackets.

firstname = input()
lastname = input()

usernametext = firstname[0].lower() + lastname[0].lower() + lastname[-1].lower()
# The [-1] index means the LAST character in the string. You could also write
# lastname[len(lastname) - 1].lower()

usernamedigit = str(len(firstname))
# The str() function converts something to a string. In this case, it's
# converting an int to a string.

username = usernametext + usernamedigit

if len(set(usernametext)) != len(usernametext):
  username += 'x'
  # This is the same as
  # username = username + 'x'

print(username)