n = int(input())
names = []

for i in range(n):
  names.append(input())

names = sorted(names)

while len(names) > 1:
  eliminator = names.pop(0)
  if len(names) == 1:
    break

  for i in range(len(eliminator)):
    j = 0
    while j < len(names):
      if eliminator[i] in names[j]:
        names.pop(j)
      j += 1

if len(names) == 1:
  print(names[0])
else:
  print("NO RESULT")